package ca;

public class Q5App {
	public static void main(String[]args){
		Q5App theApp = new Q5App();
		float average =(float) theApp.getAverageExamResult();
		System.out.println("Average exam result:" + average);
	}
	private double getAverageExamResult(){
		long count=ScannerUtility.getLong("Enter exam times:");
			do{

				for(int i=0;i<count;){
					int sum=0;
					int exam=ScannerUtility.getInt("Enter exam result:");
					if(exam>=40){
						sum+=exam;
						if(sum>100)
							System.out.println("Good job");
					}
					double average=sum/count;
					return average;
				}
			}while(count>0);	
			return count;
		}
}

