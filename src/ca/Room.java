package ca;
//import java.util.ArrayList;
import java.util.Scanner;
public class Room {
	private Scanner kb;
	//ArrayList<Room>r1=new ArrayList<>();
	private String roomNum;
	private double price;
	private int amount;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Room r1 =new Room();
		r1.getPrint();
		System.out.println(r1.toString());
	}
	public void getRoomNum(){
		//String[] roomNum={"R401"};
		System.out.print("Please enter the room number:");
		kb=new Scanner(System.in);
	    roomNum=kb.nextLine();
	}
	public void searchRoom(){
		if(roomNum.length()==0)
			System.out.println("No room");
			}
	public void getPrice(){
		//double[]price={89.99};
		System.out.print("Please enter the room price:");
		kb=new Scanner(System.in);
		price=kb.nextDouble();
	}
	public void getMaxOccupancy(){
		//int[]amount=new int[];
		System.out.print("Please enter the maximum occupancy of a room:");
		kb=new Scanner(System.in);
		amount=kb.nextInt();
	}
	public void getPrint(){
		getRoomNum();
		searchRoom();
		getPrice();
		getMaxOccupancy();
	}
	public String toString(){
		return "RoomNum:\n" + roomNum+"\nPrice:\n"+price+"\nMaximum occupancy of a room:\n"+amount;
	}
}
